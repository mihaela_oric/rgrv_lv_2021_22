import time
import vtk

from vtkmodules.vtkCommonDataModel import vtkIterativeClosestPointTransform
from vtkmodules.vtkFiltersGeneral import vtkTransformPolyDataFilter
from vtkmodules.vtkIOPLY import vtkPLYReader
from vtkmodules.vtkRenderingCore import vtkRenderer
from vtkmodules.vtkRenderingCore import vtkRenderWindow
from vtkmodules.vtkRenderingCore import vtkRenderWindowInteractor
from vtkmodules.vtkCommonDataModel import vtkPolyData
from vtkmodules.vtkIOPLY import vtkPLYReader
from vtkmodules.vtkRenderingCore import vtkPolyDataMapper
from vtkmodules.vtkRenderingCore import vtkActor
from vtkmodules.vtkRenderingCore import vtkTextActor
from vtkmodules.vtkFiltersGeneral import vtkDistancePolyDataFilter
from vtkmodules.vtkRenderingAnnotation import vtkScalarBarActor


def main():
    
    
    iterations = 500
    
    
    #Učitavanje PLY datoteka
    
    plyReader1 = vtkPLYReader()
    plyReader1.SetFileName("bunny_t4_parc.ply") #Putanja do željene datoteke
    plyReader1.Update() #Učitaj
    sourcePD = plyReader1.GetOutput() #Učitana geometrija se nalazi u vtkPolyData objektu
    
    plyReader2 = vtkPLYReader()
    plyReader2.SetFileName("bunny.ply") #Putanja do željene datoteke
    plyReader2.Update() #Učitaj
    targetPD = plyReader2.GetOutput() #Učitana geometrija se nalazi u vtkPolyData objektu
        
    
    #Mjerenje vremena u Pythonu

    start_time = time.time()
    
    
    #Iterativni algoritam najbliže točke
    
    icp = vtkIterativeClosestPointTransform()
    icp.SetSource(sourcePD) #Ulazni objekt (početna poza objekta)
    icp.SetTarget(targetPD) #Konačni objekt (željena poza objekta)
    icp.GetLandmarkTransform().SetModeToRigidBody() #Potrebni način rada je transformacija za kruta tijela
    icp.SetMaximumNumberOfIterations(iterations) #Željeni broj iteracija
    icp.SetMaximumNumberOfLandmarks(1000) #Koliko parova točaka da se koristi prilikom minimiziranja cost funkcije
    icp.Update() #Provedi algoritam
    
    icpTransformFilter = vtkTransformPolyDataFilter()
    icpTransformFilter.SetInputData(sourcePD) #Objekt s početnim koordinatama
    icpTransformFilter.SetTransform(icp) #transformiramo na novi položaj koristeći transformacijsku matricu
    icpTransformFilter.Update()
    
    icpResultPD = icpTransformFilter.GetOutput() #Transformirani (novi) objekt
    
    
    
    end_time = time.time()
    time_diff = end_time - start_time #in seconds
    
    
    actor1 = vtkActor()
    actor2 = vtkActor()
    actor3 = vtkActor()
    renderer = vtkRenderer()
    renderer.SetBackground(1.0, 1.0, 1.0) #Bijela pozadina
    
    mapper1 = vtkPolyDataMapper()
    mapper1.SetInputData(sourcePD) #Ulazni podatak je objekt tipa vtkPolyData
    actor1.SetMapper(mapper1) #Povezujemo ga s mapperom za određeni tip podataka
    actor1.GetProperty().SetColor(1.0, 0.0, 0.0) #Objekt će biti obojan u crveno
    actor1.GetProperty().SetPointSize(5) #Veličina će biti 5x5 piksela po točci

    renderer.AddActor(actor1) #Dodaj neki objekt na scenu
    
    mapper2 = vtkPolyDataMapper()
    mapper2.SetInputData(targetPD) #Ulazni podatak je objekt tipa vtkPolyData
    actor2.SetMapper(mapper2) #Povezujemo ga s mapperom za određeni tip podataka
    actor2.GetProperty().SetColor(0.0, 0.0, 1.0) #Objekt će biti obojan u crveno
    actor2.GetProperty().SetPointSize(5) #Veličina će biti 5x5 piksela po točci
    
    renderer.AddActor(actor2)
    
    mapper3 = vtkPolyDataMapper()
    mapper3.SetInputData(icpResultPD) #Ulazni podatak je objekt tipa vtkPolyData
    actor3.SetMapper(mapper3) #Povezujemo ga s mapperom za određeni tip podataka
    actor3.GetProperty().SetColor(0.0, 1.0, 0.0) #Objekt će biti obojan u crveno
    actor3.GetProperty().SetPointSize(5) #Veličina će biti 5x5 piksela po točci
    
    renderer.AddActor(actor3)
    



    renderer.ResetCamera() #Centriraj kameru tako da obuhvaća objekte na sceni


    string = 'ITERATIONS: %d\n   -> time: %.2f s' % (iterations, time_diff)
    
    
    textActor = vtkTextActor()
    textActor.SetInput(string)
    textActor.SetPosition(0, 0)
    
    textProp = textActor.GetTextProperty()
    textProp.SetFontSize(18)
    textProp.SetColor(0, 0, 1)
    
    renderer.AddActor2D(textActor)
    
    
    
    
    
    
    
    # #Distance
    
    # distanceFilter = vtk.vtkDistancePolyDataFilter()
    # distanceFilter.SetInputData(0, sourcePD)
    # distanceFilter.SetInputData(1, targetPD)
    # distanceFilter.Update()

    # distanceMap = distanceFilter.GetOutput()
    # print(distanceMap)
    
    # mapperDistance = vtkPolyDataMapper()
    # mapperDistance.SetInputConnection(distanceFilter.GetOutputPort())
    # mapperDistance.SetScalarRange(
    #     distanceFilter.GetOutput().GetPointData().GetScalars().GetRange()[0],
    #     distanceFilter.GetOutput().GetPointData().GetScalars().GetRange()[1])
    
    
    # actorDistance = vtkActor()
    # actorDistance.SetMapper(mapperDistance)
    
    # scalarBar = vtkScalarBarActor()
    # scalarBar.SetLookupTable(mapperDistance.GetLookupTable())
    # scalarBar.SetTitle("Distance")
    # scalarBar.SetNumberOfLabels(4)
    # scalarBar.UnconstrainedFontSizeOn()
    
    # renderer.AddActor2D(scalarBar)
    
    
    
  
  
    window = vtkRenderWindow()
    window.AddRenderer(renderer) #Moguće je dodati i više renderera na jedan prozor
    window.SetSize(800, 600) #Veličina prozora na ekranu
    window.SetWindowName("Scena") #Naziv prozora
    window.Render()
    
    interactor = vtkRenderWindowInteractor()
    interactor.SetRenderWindow(window)
    
    interactor.Start() #Pokretanje interaktora, potrebno kako se vtk prozor ne bi odmah zatvorio
        
    

    
if __name__ == '__main__':
    main()