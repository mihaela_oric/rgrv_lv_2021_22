import vtk
import math

import vtkmodules.vtkInteractionStyle
import vtkmodules.vtkRenderingOpenGL2
from vtkmodules.vtkCommonMath import vtkMatrix4x4

from body import Body
from display import Display



def rotate(positionMatrix, theta):
    
    #konacna matrica transformacije
    transformationMatrix = [0]*16;
    
    #matrica rotacije po Z-osi
    rotationMatrix = [
        math.cos(theta),        0,      math.sin(theta),  0.0,
        0,                      1,      0,                0.0,
        math.sin(theta) *(-1),  0,      math.cos(theta),  0.0,
        0,                      0,      0,                1.0
        ]
    
    #mnozenje matrice koja predstavlja polozaj i matrice koja rotira po z osi
    #rezultat se sprema u konacnu transformacijsku matricu
    vtkMatrix4x4().Multiply4x4(positionMatrix, rotationMatrix, transformationMatrix)

    return transformationMatrix
    
    


def main():
    
    #matrice koje odredjuju polozaj svakog tijela u odnosu na tijelo na koje su spojeni
    
    Tcylinder1 = [          #polozaj prvog cilindra
        1,  0,  0,  0.2, 
        0,  1,  0,  0.15,
        0,  0,  1,  0.0,
        0,  0,  0,  1.0
        ]
    
    Tbox2 = [               #polozaj druge kutije
        1,  0,  0,  0.3, 
        0,  1,  0,  0.3,
        0,  0,  1,  0.0,
        0,  0,  0,  1.0
        ]
    
    Tcylinder2 = [          #polozaj drugog cilindra
        1,  0,  0,  0.2, 
        0,  1,  0,  0.15,
        0,  0,  1,  0.0,
        0,  0,  0,  1.0
        ]
    
    Tbox3 = [               #polozaj treceg kvadra
        1,  0,  0,  0.3, 
        0,  1,  0,  0.3,
        0,  0,  1,  0.0,
        0,  0,  0,  1.0
        ]
    
    
    
    #kreiranje svih tijela instanciranjem klase Body()
    
    baseBox = Body()
    baseBox.CreateBox(1.2, 0.3, 0.5, 255, 0, 0)
    
    firstCylinder = Body()
    firstCylinder.CreateCylinder(0.1, 0.3, 36, 0, 0, 255)
    
    secondBox = Body()
    secondBox.CreateBox(1.2, 0.3, 0.5, 255, 0, 0)

    secondCylinder = Body()
    secondCylinder.CreateCylinder(0.1, 0.3, 36, 0, 0, 255)
    
    thirdBox = Body()
    thirdBox.CreateBox(1.2, 0.3, 0.5, 255, 0, 0)
    
    
    
    
    #korisnik unosi zeljene kutove rotacije
    while True:
        try:
            theta1 = float(input('Angle 1(degrees):'))
            theta2 = float(input('Angle 2(degrees):'))
            break
        except:
            print('ERROR')
            continue
        
    #pretvorba kuta iz stupnjeva u radijane   
    theta1 = theta1 * math.pi / 180
    theta2 = theta2 * math.pi / 180    
        
        
    #svaki kvadar (osim baseBox-a) povezan je na valjak koji se okrece
    #zbog toga se rotiraju samo ti valjci, a kvadri samo translatiraju
    
    
    #na baseBox dodajemo prvi valjak, a na taj valjak drugi kvadar
    baseBox.AddPart(firstCylinder)
    firstCylinder.AddPart(secondBox)
    #kvadar translatiramo u odnosu na valjak
    secondBox.Transform(Tbox2)
    
    #rotiranje prvog valjka
    transformationMatrixCylinder1 = [0]*16
    vtkMatrix4x4().Multiply4x4(Tcylinder1, rotate(Tcylinder1, theta1), transformationMatrixCylinder1)
    firstCylinder.Transform(transformationMatrixCylinder1) 
    
    
    #na secondBox dodajemo drugi valjak, a na taj valjak treci kvadar
    secondBox.AddPart(secondCylinder)
    secondCylinder.AddPart(thirdBox)
    #kvadar translatiramo u odnosu na valjak
    thirdBox.Transform(Tbox3)
    
    #rotiranje drugog valjka
    transformationMatrixCylinder2 = [0]*16
    vtkMatrix4x4().Multiply4x4(Tcylinder2, rotate(Tcylinder2, theta2), transformationMatrixCylinder2)
    secondCylinder.Transform(transformationMatrixCylinder2)

    
    #kreiranje displaya i dodavanje baseBoxa na display 
    #dovoljno je dodati samo baseBox, prikazat će se i ostali elementi jer su ulančano spojeni
    
    display = Display(640, 480, 'Window1', 255, 255, 255)
    baseBox.AddToDisplay(display)
    display.Run()
    
    
if __name__ == '__main__':
    main()
    
    